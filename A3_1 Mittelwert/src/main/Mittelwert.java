package main;

import java.util.*;

public class Mittelwert {

	private static Scanner scanner;

	public static void main(String[] args) {

		scanner = new Scanner(System.in);
		// (E) "Eingabe"
		// Werte f�r x und y festlegen:
		// ===========================
		double x = liesDouble("Double 1 eingeben: ");
		double y = liesDouble("Double 2 eingeben: ");
		double m;

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		// m = (x + y) / 2.0;
		m = berechneMittelwert(x, y);
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		mittelwertAusgeben(x, y, m);

		scanner.close();

	}

	public static int liesInt(String text) {
		System.out.print(text);
		return scanner.nextInt();
	}

	public static double liesDouble(String text) {
		System.out.print(text);
		return scanner.nextDouble();
	}

	public static String liesString(String text) {
		System.out.print(text);
		return scanner.nextLine();
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}

	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}

	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
	public static double reihenschaltung(double r1, double r2) {
		return r1 + r2;
	}
	
	public static double parralelschaltung(double r1, double r2) {
		return r1 * r2 / (r1 + r2);
	}
	
	public static double berechneMittelwert(double wert1, double wert2) {
		return (wert1 + wert2) / 2d;
	}

	public static void mittelwertAusgeben(double x, double y, double m) {
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
	}

}
