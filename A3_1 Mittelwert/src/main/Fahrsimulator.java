package main;

public class Fahrsimulator {
	
	public static double beschleunige(double v, double dv) {
		double neueGeschwindigkeit = dv + v;
		if(neueGeschwindigkeit > 130d) neueGeschwindigkeit = 130d;
		else if(neueGeschwindigkeit < 0d) neueGeschwindigkeit = 0d;
		System.out.println("Die neue Geschwindigkeit betr�gt " + neueGeschwindigkeit + " km/h");
		return neueGeschwindigkeit;
	}
	
}
