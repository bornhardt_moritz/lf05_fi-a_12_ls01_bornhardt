package main;

import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {
		
		aufgabe4();
		
	}
	
	private static void aufgabe4() {
		
		int[] lotto = { 3, 7, 12, 18, 37, 42 };
		
		System.out.print("[ ");
		for(int i = 0; i < lotto.length; i++) {
			System.out.print(lotto[i] + " ");
		}
		System.out.println("]");
		
		boolean enthaelt12 = false, enthaelt13 = false;
		for(int i = 0; i < lotto.length; i++) {
			if(lotto[i] == 12) enthaelt12 = true;
			else if(lotto[i] == 13) enthaelt13 = true;
		}

		System.out.println("Die Zahl 12 ist " + (enthaelt12 ? "" : "nicht ") + "in der Ziehung enthalten");
		System.out.println("Die Zahl 13 ist " + (enthaelt13 ? "" : "nicht ") + "in der Ziehung enthalten");
		
	}
	
	private static void aufgabe3() {
		
		System.out.println("Bitte 5 Ziffern eingeben");
		Scanner scanner = new Scanner(System.in);
		int[] array = { scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt(), scanner.nextInt() };
		
		for(int i = array.length - 1; i >= 0; i--) {
			System.out.println(array[i]);
		}
		
	}
	
	private static void aufgabe1() {
		
		int[] zahlen = new int[10];
		for(int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i;
		}
		
		for(int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
		
	}
	
	private static void aufgabe2() {
		
		int[] zahlen = new int[10];
		for(int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i * 2 + 1;
		}
		
		for(int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}
		
	}
	
}
