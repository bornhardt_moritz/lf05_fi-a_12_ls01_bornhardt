﻿import java.util.ArrayList;
import java.util.Scanner;

class Fahrkartenautomat {
	
	private static final String[] FAHRKARTEN = {
    		"Einzelfahrschein Berlin AB",
    		"Einzelfahrschein Berlin BC",
    		"Einzelfahrschein Berlin ABC",
    		"Kurzstrecke",
    		"Tageskarte Berlin AB",
    		"Tageskarte Berlin BC",
    		"Tageskarte Berlin ABC",
    		"Kleingruppen-Tageskarte Berlin AB",
    		"Kleingruppen-Tageskarte Berlin BC",
    		"Kleingruppen-Tageskarte Berlin ABC"
    	};
    	
    	private static final double[] PREISE = {
    		2.9d,
    		3.3d,
    		3.6d,
    		1.9d,
    		8.6d,
    		9.0d,
    		9.6d,
    		23.5d,
    		24.3d,
    		24.9d
    	};
	
    public static void main(String[] args) {

        double eingezahlterGesamtbetrag;
        double zuZahlenderBetrag;
        
        while(true) {
            
        	zuZahlenderBetrag = fahrkartenbestellungErfassen();
        	
             eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
             if(zuZahlenderBetrag > 0d) fahrkartenAusgeben();
             rueckgeldAusgeben(eingezahlterGesamtbetrag - zuZahlenderBetrag);

            System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                               "vor Fahrtantritt entwerten zu lassen!\n"+
                               "Wir wünschen Ihnen eine gute Fahrt.\n\n");
        }
        
    }
    
    private static void rueckgeldAusgeben(double rückgabebetrag) {
        if(rückgabebetrag > 0.0) {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n", Math.round(rückgabebetrag * 100) / 100.0);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");
     	   ArrayList<Double> rueckgabeMuenzen = new ArrayList<Double>();
           while(rückgabebetrag >= 2.0d) {
        	   rückgabebetrag -= 2d;
        	   rueckgabeMuenzen.add(2.0d);
        	   //rückgabebetrag -= muenzeAusgeben(2, "Euro");
           }
           while(rückgabebetrag >= 1d) {
        	   rückgabebetrag -= 1.0d;
        	   rueckgabeMuenzen.add(1.0d);
        	   //rückgabebetrag -= muenzeAusgeben(1, "Euro");
           }
           while(rückgabebetrag >= 0.5d) {
        	   rückgabebetrag -= 0.5d;
        	   rueckgabeMuenzen.add(0.5d);
        	   //rückgabebetrag -= muenzeAusgeben(50, "Cent");
           }
           while(rückgabebetrag >= 0.2d) {
        	   rückgabebetrag -= 0.2d;
        	   rueckgabeMuenzen.add(0.2d);
        	   //rückgabebetrag -= muenzeAusgeben(20, "Cent");
           }
           while(rückgabebetrag >= 0.1d) {
        	   rückgabebetrag -= 0.1d;
        	   rueckgabeMuenzen.add(0.1d);
        	   //rückgabebetrag -= muenzeAusgeben(10, "Cent");
           }
           while(rückgabebetrag >= 0.05d) {
        	   rückgabebetrag -= 0.05d;
        	   rueckgabeMuenzen.add(0.05d);
        	   //rückgabebetrag -= muenzeAusgeben(5, "Cent");
           }
           muenzenAusgeben(rueckgabeMuenzen);
        }
    }
    
    private static void muenzenAusgeben(ArrayList<Double> muenzen) { //gibt den Betrag als double zurück
    	for(int i = 0; i < muenzen.size(); i++) {
    		double muenze = muenzen.get(i);
        	System.out.println("     ***     ");
        	System.out.println("   **   **   ");
        	System.out.println(" **   " + (muenze >= 1d ? (int)muenze : (int)(muenze * 100d)) + "   ** ");
        	System.out.println("*   " + (muenze >= 1d ? "Euro" : "Cent") + "    *");
        	System.out.println(" **       ** ");
        	System.out.println("   **   **   ");
        	System.out.println("     ***     ");
    	}
    }
    
    /*private static double muenzeAusgeben(int betrag, String einheit) { //gibt den Betrag als double zurück
    	System.out.println("     ***     ");
    	System.out.println("   **   **   ");
    	System.out.println(" **   " + betrag + "   ** ");
    	System.out.println("*   " + einheit + "    *");
    	System.out.println(" **       ** ");
    	System.out.println("   **   **   ");
    	System.out.println("     ***     ");
    	return betrag / (einheit.equals("Euro") ? 1d : 100d); //ist die Einheit nicht Euro, wird der Betrag durch 100 geteilt
    }*/
    
    private static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for(int i = 0; i < 8; i++) {
           System.out.print("=");
           warte(250);
        }
        System.out.println("\n\n");
    }
    
    private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0d, eingeworfeneMünze;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", Math.round((zuZahlenderBetrag - eingezahlterGesamtbetrag) * 100) / 100.0);
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
     	   if(eingeworfeneMünze != 0.05d && eingeworfeneMünze != 0.1d && eingeworfeneMünze != 0.2d && eingeworfeneMünze != 0.5d &&
     			   eingeworfeneMünze != 1d && eingeworfeneMünze != 2d) {
     		   System.out.println(eingeworfeneMünze + " ist keine gültige Münze!");
     	   }
     	   else {
     		  eingezahlterGesamtbetrag += eingeworfeneMünze;
     	   }
        }
        return eingezahlterGesamtbetrag;
        
    }
    
    private static double fahrkartenbestellungErfassen() {
    	
    	Scanner tastatur = new Scanner(System.in);
        
        double zuZahlenderBetrag = 0d; 
        short anzahlFahrkarten;
        byte gewaehlteFahrkarte;
        
        while(true) {
        	
            System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
            for(int i = 0; i < FAHRKARTEN.length; i++) {
            	System.out.printf("\t" + FAHRKARTEN[i] + " [%.2f €] (" + (i + 1) + ")\n", PREISE[i]);
            }
            System.out.println("\tZahlen (99)");
            
            System.out.print("Ihre Wahl: ");
            gewaehlteFahrkarte = tastatur.nextByte();
            
            if(gewaehlteFahrkarte < 1 || (gewaehlteFahrkarte > FAHRKARTEN.length && gewaehlteFahrkarte != 99))
            	System.out.println(">>Falsche Eingabe<<");
            else if(gewaehlteFahrkarte == 99) break;
            
            while(true) {
            	
                System.out.print("Anzahl Fahrkarten: ");
                anzahlFahrkarten = tastatur.nextShort();
            	
                if(anzahlFahrkarten < 1 || anzahlFahrkarten > 10) System.out.println(">>Bitte wählen sie 1 bis 10 Fahrkarten<<");
                else break;
                
            }
            
            zuZahlenderBetrag += PREISE[gewaehlteFahrkarte - 1] * anzahlFahrkarten;
            
            System.out.printf("Zwischensumme: %.2f Euro\n", Math.round(zuZahlenderBetrag * 100) / 100d);
        	
        }
        
        return zuZahlenderBetrag;
        
    }
    
    private static void warte(int millis) {
        try {
			Thread.sleep(millis);
		} catch (InterruptedException e) { e.printStackTrace(); }
    }
}

/*
zu 5.: der verwendete Datentyp ist short, da er ganzzahlig ist und die Größe mit 2 Byte ausreichend ist
zu 6.: bei der Berechnung anzahl * einzelpreis wird der Einzelpreis der Karte mit der Anzahl multipliziert; der Wert muss
	allerdings in einer Variable gespeichert werden, weshalb ich += geschreiben habe

Der Vorteil, wenn Arrays benutzt werden ist, dass viel einfacher Fahrkarten hinzugefügt oder gelöscht werden können, da für eine
neue Fahrkarte nur ein Eintrag ins Array benötigt wird. Außerdem spart man sich viel Schreibarbeit, da das Array durch for-Schleifen
automatisch ausgelesen wird

*/