package main;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		
		aufgabe2();
		
	}
	
	private static void aufgabe2() {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("Nettowert eingeben: ");
		double nettowert = scanner.nextDouble();
		System.out.print("Erm��igter Steuersatz? (J/N) ");
		char in = scanner.next().charAt(0);
		
		if(in == 'J') System.out.println(nettowert);
		else if(in == 'N') System.out.println(nettowert * 1.19d);
		
		scanner.close();
		
	}
	
	private static void aufgabe1mit2zahlen() {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("2 Zahlen eingeben: ");
		int zahl1 = scanner.nextInt(),
			zahl2 = scanner.nextInt();
		
		//1. Aufgabe: wenn Zeit nach 15.20, dann fahre S-Bahn, sonst fahre U-Bahn
		
		//2. Aufgabe
		if(zahl1 == zahl2) System.out.println("Beide Zahlen sind identisch");
		
		//3. Aufgabe
		if(zahl2 > zahl1) System.out.println("Zahl 2 ist gr��er als Zahl 1");
		
		//4. Aufgabe
		if(zahl1 >= zahl2) System.out.println("Zahl 1 ist gr��er/gelich Zahl 2");
		else System.out.println("Zahl 2 ist gr��er");
		
		scanner.close();
		
	}
	
	private static void aufgabe1mit3zahlen() {
		
		Scanner scanner = new Scanner(System.in);
		
		System.out.print("3 Zahlen eingeben: ");
		int zahl1 = scanner.nextInt(),
			zahl2 = scanner.nextInt(),
			zahl3 = scanner.nextInt();
		
		//Aufgabe 1 mit 3 Zahlen
		if(zahl1 > zahl2 && zahl1 > zahl3) System.out.println("Erste Zahl am gr��ten");
		
		//Aufgabe 2 mit 3 Zahlen
		if(zahl3 > zahl1 || zahl3 > zahl2) System.out.println("3. Zahl nicht am kleinsten");
		
		//Aufgabe 3 mit 3 Zahlen
		if(zahl1 > zahl2 && zahl1 > zahl3) System.out.println("Zahl 1 am gr��ten");
		else if(zahl2 > zahl3) System.out.println("Zahl 2 am gr��ten");
		else System.out.println("Zahl 3 am gr��ten");
		
		scanner.close();
		
	}
	
}
