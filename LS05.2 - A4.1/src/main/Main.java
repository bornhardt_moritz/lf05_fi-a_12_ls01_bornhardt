package main;

import java.util.ArrayList;

/**
 * Das ist die Startklasse des Programms, hier wird das Programm gestartet
 */
public class Main {
	
	/**
	 * Diese Methode initialisiert alle Objekte und ruft Funktionen auf
	 * @param args
	 */
	public static void main(String[] args) {

		Ladung l1 = new Ladung("Ferengi Schneckensaft", 200);
		Ladung l2 = new Ladung("Borg-Schrott", 5);
		Ladung l3 = new Ladung("Rote Materie", 2);
		Ladung l4 = new Ladung("Forschungssonde", 35);
		Ladung l5 = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung l6 = new Ladung("Plasma-Waffe", 50);
		Ladung l7 = new Ladung("Photonenorpedo", 3);
		
		Raumschiff klingonen = new Raumschiff(3, 100, 100, 100, 100, 2, "IKS Hegh'ta", new ArrayList<String>(), new ArrayList<Ladung>());
		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, 2, "IRW Khazara", new ArrayList<String>(), new ArrayList<Ladung>());
		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, 5, "Ni'Var", new ArrayList<String>(), new ArrayList<Ladung>());
		
		klingonen.addLadung(l1);
		klingonen.addLadung(l5);

		romulaner.addLadung(l2);
		romulaner.addLadung(l3);
		romulaner.addLadung(l6);
		
		vulkanier.addLadung(l4);
		vulkanier.addLadung(l7);
		
		klingonen.photonentorpedoSchiessen(romulaner);
		romulaner.phaserKanoneSchiessen(klingonen);
		vulkanier.nachrichtAnAlle("Gewalt ist nicht logisch");
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		vulkanier.reparaturAndroidenEinsetzen(true, true, true, false, vulkanier.getAndroidenAnzahl());
		vulkanier.ladungPhotonentorpedosEinsetzen(100);
		vulkanier.ladungsverzeichnisAufraeumen();
		klingonen.phaserKanoneSchiessen(romulaner);
		klingonen.phaserKanoneSchiessen(romulaner);
		klingonen.zustandRaumschiff();
		klingonen.ladungsverzeichnisAusgeben();
		romulaner.zustandRaumschiff();
		romulaner.ladungsverzeichnisAusgeben();
		vulkanier.zustandRaumschiff();
		vulkanier.ladungsverzeichnisAusgeben();
		klingonen.eintraegeLogbuchZurueckgeben(); //gibt den Broadcast Kommunikator aus, der alle Nachrichten an alle speichert
		
	}
	
}
