package main;

import java.util.ArrayList;
import java.util.Random;

/**
 * 
 * Die Klasse Raumschiff, die alle Attribute und Methoden zu diesem bereitstellt.
 * 
 * @author Bornhardt
 *
 */
public class Raumschiff {
	
	private int photonentorpedoAnzahl;
	private int energieversorungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	
	/**
	 * Initialisiert das Raumschiff mit Standardwerten
	 */
	public Raumschiff() {
		this.photonentorpedoAnzahl = 0;
		this.energieversorungInProzent = 0;
		this.schildeInProzent = 0;
		this.huelleInProzent = 0;
		this.lebenserhaltungssystemeInProzent = 0;
		this.androidenAnzahl = 0;
		this.schiffsname = "";
		this.broadcastKommunikator = new ArrayList<String>();
		this.ladungsverzeichnis = new ArrayList<Ladung>();
	}
	
	/**
	 * Initialisiert das Raumschiff mit �bergebenen Werten
	 * @param photonentorpedoAnzahl
	 * @param energieversorungInProzent
	 * @param schildeInProzent
	 * @param huelleInProzent
	 * @param lebenserhaltungssystemeInProzent
	 * @param androidenAnzahl
	 * @param schiffsname
	 * @param broadcastKommunikator
	 * @param ladungsverzeichnis
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname,
			ArrayList<String> broadcastKommunikator, ArrayList<Ladung> ladungsverzeichnis) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorungInProzent = energieversorungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		this.broadcastKommunikator = broadcastKommunikator;
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorungInProzent() {
		return energieversorungInProzent;
	}

	public void setEnergieversorungInProzent(int energieversorungInProzent) {
		this.energieversorungInProzent = energieversorungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	public void addLadung(Ladung ladung) {
		ladungsverzeichnis.add(ladung);
	}
	
	/**
	 * Schie�t einen Torpedo, wenn es mehr als 0 gibt. Sie sendet immer eine Nachricht an alle.
	 * @param r
	 */
	public void photonentorpedoSchiessen(Raumschiff r) {
		if(photonentorpedoAnzahl == 0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			photonentorpedoAnzahl--;
			nachrichtAnAlle("Photonentorpedo abgeschossen");
			treffer(r);
		}
	}
	
	/**
	 * Schie�t die Kanone, wenn die Energieversorgung >= 49 ist. Sie sendet immer eine Nachricht an alle.
	 * @param r
	 */
	public void phaserKanoneSchiessen(Raumschiff r) {
		if(energieversorungInProzent < 50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			energieversorungInProzent -= 50;
			nachrichtAnAlle("Phaserkanone abgeschossen");
			treffer(r);
		}
	}
	
	/**
	 * Das Raumschiff wurde getroffen und gibt den Treffer aus.
	 * @param r
	 */
	private void treffer(Raumschiff r) {
		System.out.printf("%s wurde getroffen!%n", r.getSchiffsname());
		r.trefferVermerken();
	}
	
	/**
	 * Das Raumschiff bekommt den Schaden des Treffers abgezogen
	 */
	public void trefferVermerken() {
		schildeInProzent -= 50;
		if(schildeInProzent <= 0) {
			huelleInProzent -= 50;
			energieversorungInProzent -= 50;
		}
		if(huelleInProzent <= 0) {
			lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle("Lebenserhaltungssysteme vollst�ndig zerst�rt!");
		}
	}
	
	/**
	 * Sendet eine Nachricht an alle
	 * @param message
	 */
	public void nachrichtAnAlle(String message) {
		System.out.println(message);
		broadcastKommunikator.add(message);
	}
	
	/**
	 * Gibt jeden Eintrag des Logbuches in der Konsole aus
	 * @return the broadcast communicator
	 */
	public ArrayList<String> eintraegeLogbuchZurueckgeben()	{
		for(String s : broadcastKommunikator) {
			System.out.println(s);
		}
		return broadcastKommunikator;
	}
	
	/**
	 * L�dt die �bergebene Anzahl an Torpedos ein
	 * @param anzahlTorpedos
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {
		this.photonentorpedoAnzahl += anzahlTorpedos;
	}
	
	/**
	 * Gibt den Status des Raumschiffes in der Konsole aus
	 */
	public void zustandRaumschiff() {
		System.out.printf("PhotonentorpedoAnzahl: %d%nEnergieversorgung: %d%%%nSchilde: %d%%%nH�lle: %d%%%nLebenserhaltungssysteme: %d%%%nAnzahl Androiden: %d%nSchiffsname: %s%n",
				photonentorpedoAnzahl, energieversorungInProzent, schildeInProzent, huelleInProzent,
				lebenserhaltungssystemeInProzent, androidenAnzahl, schiffsname);
	}
	
	/**
	 * Gibt alle Ladungen mit Bezeichnung und Menge aus
	 */
	public void ladungsverzeichnisAusgeben() {
		for(Ladung l : ladungsverzeichnis) {
			System.out.printf("Bezeichnung: %s, Menge: %d%n", l.getBezeichnung(), l.getMenge());
		}
	}
	
	/**
	 * L�scht alle Ladungen
	 */
	public void ladungsverzeichnisAufraeumen() {
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			if(ladungsverzeichnis.get(i).getMenge() <= 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
	
	/**
	 * Setzt die �bergebene Anzahl an Torpedos ein
	 * @param anzahl
	 */
	public void ladungPhotonentorpedosEinsetzen(int anzahl) {
		Ladung torpedo = null;
		for(Ladung ladung : ladungsverzeichnis) {
			if(ladung.getBezeichnung().equals("Photonentorpedos")) torpedo = ladung;
		}
		if(torpedo == null) return;
		int anzahlEingesetzteTorpedos = 0;
		for(int i = 0; i < anzahl; i++) {
			if(torpedo.getMenge() > 0) {
				torpedo.setMenge(torpedo.getMenge() - 1);
				photonentorpedoAnzahl++;
				anzahlEingesetzteTorpedos++;
			}
		}
		if(anzahlEingesetzteTorpedos > 0) System.out.printf("%n Torpedos eingesetzt!", anzahlEingesetzteTorpedos);
	}
	
	/**
	 * Repariert das Schiff mithilfe von Adroiden
	 * @param energieversorgung
	 * @param schilde
	 * @param huelle
	 * @param lebenserhaltungssysteme
	 * @param anzahlAndroiden
	 */
	public void reparaturAndroidenEinsetzen(boolean energieversorgung, boolean schilde, boolean huelle,
			boolean lebenserhaltungssysteme, int anzahlAndroiden) {
		
		int nutzbareAndroiden = anzahlAndroiden > this.androidenAnzahl ? this.androidenAnzahl : anzahlAndroiden;
		int zufallszahl = new Random().nextInt(100);
		int anzahlSchiffsstrukturen = 0;

		if(energieversorgung) anzahlSchiffsstrukturen++;
		if(schilde) anzahlSchiffsstrukturen++;
		if(huelle) anzahlSchiffsstrukturen++;
		if(lebenserhaltungssysteme) anzahlSchiffsstrukturen++;
		
		int prozentualeReparatur = (int)(zufallszahl * nutzbareAndroiden / (float)anzahlSchiffsstrukturen);
		
		if(energieversorgung) energieversorungInProzent += prozentualeReparatur;
		if(schilde) schildeInProzent += prozentualeReparatur;
		if(huelle) huelleInProzent += prozentualeReparatur;
		if(lebenserhaltungssysteme) lebenserhaltungssystemeInProzent += prozentualeReparatur;
		
	}
	
}
