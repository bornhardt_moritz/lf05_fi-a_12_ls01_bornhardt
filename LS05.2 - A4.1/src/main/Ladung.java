package main;

/**
 * 
 * Speichert eine Ladung mit Bezeichnung und Menge
 * 
 * @author Bornhardt
 *
 */
public class Ladung {
	
	private String bezeichnung;
	private int menge;
	
	/**
	 * Initialisiert die Klasse mit Standardwerten
	 */
	public Ladung() {
		this.bezeichnung = "";
		this.menge = 0;
	}
	
	/**
	 * Initialisiert die Klasse mit übergebenen Werten
	 * @param bezeichnung
	 * @param menge
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge;
	}
	
	/**
	 * Überschreibt die toString() Funktion. Wird die Methode aufgerufen, gibt sie Bezeichnung und Menge aus
	 */
	@Override
	public String toString() {
		return "Bezeichnung: " + bezeichnung + ", Menge: " + menge;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public int getMenge() {
		return menge;
	}

	public void setMenge(int menge) {
		this.menge = menge;
	}
	
}
