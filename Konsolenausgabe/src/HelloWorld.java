public class HelloWorld {
	
	public static void main(String[] args) {
		aufgabe1();
		aufgabe2();
		aufgabe3();
	}
	
	private static void aufgabe1() {
		//die print() Funktion erstellt im Gegensatz zur println() keinen Zeilenumbruch
		System.out.println("Das ist ein \"Satz\".\n" + "Das auch!");
	}
	
	private static void aufgabe2() {
		System.out.printf("%9s\n", "*");
		System.out.printf("%10s\n", "***");
		System.out.printf("%11s\n", "*****");
		System.out.printf("%12s\n", "*******");
		System.out.printf("%13s\n", "*********");
		System.out.printf("%14s\n", "***********");
		System.out.printf("%15s\n", "*************");
		System.out.printf("%10s\n", "***");
		System.out.printf("%10s\n", "***");
	}
	
	private static void aufgabe3() {
		System.out.printf("%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
	}
	
}
