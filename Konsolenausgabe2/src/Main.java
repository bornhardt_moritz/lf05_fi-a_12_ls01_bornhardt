
//@author: Moritz Bornhardt Fi-A 12

public class Main {
	
	public static void main(String[] args) {
		//aufgabe1();
		//aufgabe2();
		//aufgabe3();
		//WeltDerZahlen.main();
		Variablen.main();
	}
	
	private static void aufgabe1() {
		System.out.printf("%5s\n", "**");
		System.out.printf("%1s%7s\n\n", "*", "*");
		System.out.printf("%1s%7s\n", "*", "*");
		System.out.printf("%5s", "**");
	}
	
	private static void aufgabe2() {
		System.out.printf("%-5s=%-19s=%4d\n", "0!", "", 1);
		System.out.printf("%-5s=%-19s=%4d\n", "1!", "1", 1);
		System.out.printf("%-5s=%-19s=%4d\n", "2!", "1 * 2", 2);
		System.out.printf("%-5s=%-19s=%4d\n", "3!", "1 * 2 * 3", 6);
		System.out.printf("%-5s=%-19s=%4d\n", "4!", "1 * 2 * 3 * 4", 24);
		System.out.printf("%-5s=%-19s=%4d\n", "5!", "1 * 2 * 3 * 4 * 5", 120);
	}
	
	private static void aufgabe3() {
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celcius");
		System.out.printf("-----------------------\n");
		System.out.printf("%-12d|%10.2f\n", -20, -28.8889f);
		System.out.printf("%-12d|%10.2f\n", -10, -23.3333);
		System.out.printf("%-12d|%10.2f\n", 0, -17.7778);
		System.out.printf("%-12d|%10.2f\n", 20, -6.6667);
		System.out.printf("%-12d|%10.2f\n", 30, -1.1111);
	}
	
}
