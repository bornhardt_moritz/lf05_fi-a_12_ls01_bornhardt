package main;

import java.util.*;

public class Main {
	
	public static void main(String[] args) {
		
		aufgabe5();
		
	}
	
	private static void aufgabe1a() {
		
		System.out.print("Bitte n eingeben: ");
		int n = new Scanner(System.in).nextInt();
		
		for(int i = 1; i <= n; i++) {
			System.out.println(i);
		}
		
	}
	
	private static void aufgabe1b() {
		
		System.out.print("Bitte n eingeben: ");
		int n = new Scanner(System.in).nextInt();
		
		for(int i = n; i >= 1; i--) {
			System.out.println(i);
		}
		
	}
	
	private static void aufgabe2a() {
		
		System.out.print("Bitte n eingeben: ");
		int n = new Scanner(System.in).nextInt();
		
		int ergebnis = 0;
		for(int i = 1; i <= n; i++) {
			ergebnis += i;
		}
		
		System.out.println(ergebnis);
		
	}
	
	private static void aufgabe2b() {
		
		System.out.print("Bitte n eingeben: ");
		int n = new Scanner(System.in).nextInt();
		
		int ergebnis = 0;
		for(int i = 1; i <= n; i++) {
			ergebnis += i * 2;
		}
		
		System.out.println(ergebnis);
		
	}
	
	private static void aufgabe2c() {
		
		System.out.print("Bitte n eingeben: ");
		int n = new Scanner(System.in).nextInt();
		
		int ergebnis = 0;
		for(int i = 0; i <= n; i++) {
			ergebnis += i * 2 + 1;
		}
		
		System.out.println(ergebnis);
		
	}
	
	private static void aufgabe3() {
		
		for(int i = 0; i <= 200; i++) {
			if(i % 7 == 0 || (i % 5 != 0 && i % 4 == 0)) System.out.println(i);
		}
		
	}
	
	private static void aufgabe4() {
		
		for(int i = 99; i >= 9; i -= 3) {
			System.out.println(i);
		}
		
		for(int i = 1; i <= 400; i += 3) {
			System.out.println(i);
		}
		
		for(int i = 2; i <= 102; i += 4) {
			System.out.println(i);
		}
		
		int a = 4;
		for(int i = 4; i <= 1024; i += a) {
			System.out.println(i);
			a += 8;
		}
		
		for(int i = 2; i <= 32768; i += 0) {
			System.out.println(i);
			i *= 2;
		}
		
	}
	
	private static void aufgabe5() {
		for(int i = 1; i <= 10; i++) {
			for(int j = 1; j <= 10; j++) {
				System.out.println(i + " x " + j + " = " + i * j);
			}
		}
	}
	
}
